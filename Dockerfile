FROM registry.fedoraproject.org/fedora-minimal:38
LABEL maintainer="Alexandre Flion <huntears@kreog.com>"

ENV LANG=en_US.utf8 LANGUAGE=en_US:en LC_ALL=en_US.utf8

RUN microdnf -y upgrade

RUN microdnf -y --refresh install   \
        --setopt=tsflags=nodocs     \
        --setopt=deltarpm=false     \
        clang                       \
        make                        \
        cmake                       \
        patch                       \
        tar                         \
        curl                        \
        git                         \
        gtest-devel                 \
        rpm-build                   \
        CSFML-devel                 \
        dpkg                        \
        unzip

RUN microdnf clean all

RUN cd /tmp \
    && rm -rf /tmp/* \
    && chmod 1777 /tmp

WORKDIR /usr/app
